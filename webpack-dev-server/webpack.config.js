const path = require('path')
const ExtractTextPlugin = require('mini-css-extract-plugin');

const config = {
  mode: 'development',
  entry: {
    "home": path.resolve(__dirname, 'src/js/index.js') 
  },
  output :{ // Salida 
    path: path.resolve(__dirname, 'dist'), // creea el directorio donde se va compilar el JS
    filename: 'js/[name].js' // archivo "compilado" que va a geberar webpack
  },
  module: {
    rules: [{
      // Aquí van los loadres
      // test: que  tipo de archivo quiero reconocer
      // use:  que loader se va a encargar del archivo.
      test: /\.css$/,
      use: [ 
        ExtractTextPlugin.loader,
        "css-loader"
      ]
    }]
  },
  plugins: [ // En esta sección se aggregan los plugins!!
    new ExtractTextPlugin({
      // [name] toma el nombre del entry, en este caso estilos o main cuando es solo un unico entry!
      filename: "css/[name].css",
      // chunkFilename: "estilos.css"
    })
  ]
}

module.exports = config