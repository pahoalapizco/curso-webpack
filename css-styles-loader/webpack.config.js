const path = require('path')

const config = {
  mode: 'development',
  entry: path.resolve(__dirname, 'index.js'), // Archivo de inicio
  output :{ // Salida 
    path: path.resolve(__dirname, 'dist'), // creea el directorio donde se va compilar el JS
    filename: 'bundle.js' // archivo "compilado" que va a geberar webpack
  },
  module: {
    rules: [{
      // Aquí van los loadres
      // test: que  tipo de archivo quiero reconocer
      // use:  que loader se va a encargar del archivo.
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    }]
  }
}

module.exports = config